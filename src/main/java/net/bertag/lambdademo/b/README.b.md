# Part B

## The null problem

The Java language supports the null literal as a mechanism for describing the absence of a value or assignment.  But
null values do not behave in the same manner as a non-null object.  Printing null **String**s, for example, results in
the word "null" being printed.  And attempting to call methods on a null value, even if such methods are supported by
the type of variable being referenced, will always result in a **NullPointerException**.

Therefore, appropriately handling null references is a necessary part of any Java development, and there are many
strategies to help developers approach it.  Some of these strategies include the following:

- Using sane defaults if no values are defined (e.g.: use empty collections instead of null collections).
- Explicitly documenting when nulls are acceptable as method arguments and/or return values.
- Checking for null values before using an object, especially when documentation indicates that a null value should be
  reasonably expected.

To help facilitate management of null values, Java 8 introduces a new class, **Optional**, that provides several clever
mechanisms for working with potentially null data.

## Working with Optionals

The **Optional** class wraps up null values in such a way that an "empty" **Optional** represents a null, while a
"non-empty" **Optional** represents a non-null value.  The **Optional** class has no public constructor; instead it
is built by using one of 3 static builder methods:

- `Optional.empty()`: Used when it is known that the resulting **Optional** will always be empty.
- `Optional.of(T value)`: Used when it is known that the resulting **Optional** will always be non-empty.
- `Optional.ofNullable(T value)`: Used when it is unknown whether the resulting **Optional** will be empty or non-empty.

With that in mind, let us consider an example.  In family history, it is conventional to write names in their
natural order (e.g.: first, middle, last).  Additionally, maiden names are used in place of married names where
appropriate; finally, surnames (either maiden or married) are capitalized.

Open `OldSchool.java`, which provides an inner **Person** class with fields for first, middle, maiden, and last names.
Notice that the `naiveToString` method does not make any effort to handle the possibility of null values (except when
deciding whether to use a maiden name as the surname).  As a result, we get unexpected output as well as a 
**NullPointerException**.  This is the worst case scenario, and with some thought, we can avoid it by use sane defaults
(in this case, empty strings) when we encounter null **String**s, as demonstrated by the `safeToString` method.

Compare this solution to the one provided in `NewSchool.java`.  Rather than returning **String** objects directly, note
that the **Person** getter methods now return `Optional<String>`.  This informs any downstream user that it is reasonable
and even expected that the method might return an effectively null value, represented by an empty **Optional**.

_It is worth noting that it is still possible to effectively create another "naive" solution to resolving the 
**Optional**s, by blindly calling `Optional.get` on each one, which would throw an exception if the **Optional** is
empty.  But unlike the naive example in our previous class, which failed due to simple oversight, blindly calling `get` 
amounts to gross negligence.  In essence, despite being clearly warned to expect optional values, we have ignored that 
warning._

But it is also easy to look at **Optional** and wonder what its use accomplishes.  The most basic usage of **Optional**
is demonstrated by the `clumsyToString` method and is essentially a rewrite of the `safeToString` method in `OldSchool.java`.
We read in each value from the **Person** and evaluate whether it is safe to use or not, effectively trading
`val == null` for `val.isPresent()`.  The keen observer will notice, however, that in practice, this method of resolving
**Optional**s is more verbose than the old null checks, and reduces readability overall.

So it is important to learn that this is only one of several mechanisms by which we can resolve **Optional** values, and
one that we will likely not use very often.  Instead consider the pattern introduced in `elegantToString`:

- Use `Optional.map` to define transformations (written as Lambda expressions) for the wrapped data, if it exists.
  In our example, we append a space to the end of each name if it exists.  Note that `map` is only called if the
  **Optional** is non-empty; `map` can also be called multiple times if there are transformation algorithms best
  expressed as a series of discrete steps.
- Use `Optional.orElse` to return the wrapped value (as of the latest transformation) if the **Optional** is non-empty,
  or the provided default value if it is empty.  In our example, for first, middle, and last names, we use the empty
  string as the "sane default."
- `Optional.orElseGet` provides an alternative to `orElse` with one subtle difference.  `orElse` defines a default _value_;
  if that default value is expressed as a method call or other potentially expensive evaluation, it will be executed in
  _every_ case (regardless of whether the **Optional** is empty or not).  `orElseGet` instead defines an _algorithm_
  (expressed as a lambda function) for calculating the default value; the lambda will only be executed/resolved if the
  **Optional** is empty.  As a rule of thumb, use `orElse` for situations where the "sane default" is a constant or is
  cheaply calculated.  Use `orElseGet` when calculating the "sane default" is expensive.  In our example, we use the
  latter to determine the default value if maiden name is empty, since it involves several other method calls.

**Optional** defines several other useful methods, all of which accept lambda expressions as their arguments.
`Optional.orElseThrow`, for example, provides a mechanism for quickly constructing an custom exception to be thrown in the
case that the **Optional** is empty.  `filter` and `flatMap` will be introduced when we discuss working with Streams.
If we consider an **Optional** to be similar to a **Stream** containing 0 or 1 elements, they have identical 
functionality.  This same relationship also relates `Optional.ifPresent` and `Stream.forEach`.

All of the methods in **Optional** are useful in particular situations (even the so-called "clumsy" `isPresent` method
discussed earlier), and as you work with this versatile class, you will learn when to use each approach.  At the end of
the day, however, **Optional** provides another powerful tool in your belt for resolving potentially null values.

## When to use Optionals (and when not to...)

**Optional**s have been specifically designed by the Java maintainers for use as transient return value objects.  They
do not implement **Serializable** and are not intended to be saved as class variables, provided as method arguments,
and so forth.  However, there are some specific circumstances where you might deliberately choose to violate these
guidelines; for example, the `NewSchool.elegantToString` method has duplicated code, and it may be appropriate for us to 
instead define a private helper method:

```java
private String resolve(Optional<String> optionalString) {
    return optionalString
    		.map(str -> str + " ")
    		.orElse("");
}
```

**Optional**s may also be useful for managing multi-step transformations on possibly null values.  For example, JSON
does not define a discrete date/time class; dates are stored as strings instead.  But in order to perform calendar
math, we may wish to convert the JSON string to a proper `java.time` object.  In such a context, using the Optional class
provides an elegant and safe mechanism for performing the conversion:

```java
Instant date = Optional.ofNullable(jsonNode.path("date").asText(null))
		.map(LocalDate::parse)
		.map(localDate -> localDate.atStartOfDay(ZoneId.systemDefault()))
		.map(ZonedDateTime::toInstant)
		.orElse(null);
```

While these examples provide practical illustrations of using **Optional**s outside the context of return values,
understand that they do not necessarily represent "good practice".  Indeed, according to some developers they represent
an anti-pattern that should be avoided altogether. Regardless of where you draw the line on "appropriate" **Optional**
usage, the general rule of thumb is that **Optional** objects should not be persisted and should be resolved as quickly
and as locally as possible.

It is also worth noting that the use of **Optional** values in getter methods as demonstrated in this lesson violates
the long-standing Java Bean pattern, which benefits from widespread support from many different third-party libraries.
It remains to be seen how the Java community will adapt to the creation of this new class, but in the meantime, you may
sometimes have to choose between the functional paradigms introduced by Java 8 and the OOP paradigms for which
traditional Java became famous.