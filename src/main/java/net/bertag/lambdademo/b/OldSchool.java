package net.bertag.lambdademo.b;

public class OldSchool {
    
    public static void main(String[] args) {
        Person person1 = new Person("Benjamin", "Lafayette", null, "Sisko");
        Person person2 = new Person("Kasidy", null, "Yates", "Sisko");
        Person person3 = new Person("Jake", null, null, "Sisko");
        Person person4 = new Person("Dukat", null, null, null);
        
        try {
            System.out.println(naiveToString(person1));
            System.out.println(naiveToString(person2));
            System.out.println(naiveToString(person3));
            System.out.println(naiveToString(person4));
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        
        System.out.println();
        
        System.out.println(safeToString(person1));
        System.out.println(safeToString(person2));
        System.out.println(safeToString(person3));
        System.out.println(safeToString(person4));
    }
    
    private static String naiveToString(Person person) {
        return person.getFirstName() + " "
                + person.getMiddleName() + " "
                + (person.getMaidenName() != null ? person.getMaidenName() : person.getLastName()).toUpperCase();
    }
    
    private static String safeToString(Person person) {
        String firstName = person.getFirstName() != null ? person.getFirstName() + " " : "";
        String middleName = person.getMiddleName() != null ? person.getMiddleName() + " " : "";
        String maidenName = person.getMaidenName() != null ? person.getMaidenName() : "";
        String lastName = person.getLastName() != null ? person.getLastName() : "";
        
        return firstName + middleName + (maidenName.isEmpty() ? lastName : maidenName).toUpperCase();
    }
    
    public static class Person {
        
        private final String firstName;
        private final String middleName;
        private final String maidenName;
        private final String lastName;
        
        public Person(String firstName, String middleName, String maidenName, String lastName) {
            this.firstName = firstName;
            this.middleName = middleName;
            this.maidenName = maidenName;
            this.lastName = lastName;
        }
        
        /**
         * @return the firstName
         */
        public String getFirstName() {
            return firstName;
        }
        
        /**
         * @return the middleName
         */
        public String getMiddleName() {
            return middleName;
        }
        
        /**
         * @return the maidenName
         */
        public String getMaidenName() {
            return maidenName;
        }
        
        /**
         * @return the lastName
         */
        public String getLastName() {
            return lastName;
        }
        
    }
    
}
