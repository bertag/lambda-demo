package net.bertag.lambdademo.b;

import java.util.Optional;

public class NewSchool {
    
    public static void main(String[] args) {
        Person person1 = new Person("Benjamin", "Lafayette", null, "Sisko");
        Person person2 = new Person("Kasidy", null, "Yates", "Sisko");
        Person person3 = new Person("Jake", null, null, "Sisko");
        Person person4 = new Person("Dukat", null, null, null);
        
        System.out.println(clumsyToString(person1));
        System.out.println(clumsyToString(person2));
        System.out.println(clumsyToString(person3));
        System.out.println(clumsyToString(person4));
        
        System.out.println(elegantToString(person1));
        System.out.println(elegantToString(person2));
        System.out.println(elegantToString(person3));
        System.out.println(elegantToString(person4));
    }

    private static String clumsyToString(Person person) {
        String firstName = person.getFirstName().isPresent() ? person.getFirstName().get() + " " : "";
        String middleName = person.getMiddleName().isPresent() ? person.getMiddleName().get() + " " : "";
        String maidenName = person.getMaidenName().isPresent() ? person.getMaidenName().get() : "";
        String lastName = person.getLastName().isPresent() ? person.getLastName().get() : "";

        return firstName + middleName + (maidenName.isEmpty() ? lastName : maidenName).toUpperCase();
    }
    
    private static String elegantToString(Person person) {
        return person.getFirstName()
                        .map(str -> str + " ")
                        .orElse("")
                + person.getMiddleName()
                        .map(str -> str + " ")
                        .orElse("")
                + person.getMaidenName()
                        //.orElse(person.getLastName().orElse(""))  // This could also work.  See the README.
                        .orElseGet(() -> person.getLastName().orElse(""))
                        .toUpperCase();
    }
    
    public static class Person {
        
        private final String firstName;
        private final String middleName;
        private final String maidenName;
        private final String lastName;
        
        public Person(String firstName, String middleName, String maidenName, String lastName) {
            this.firstName = firstName;
            this.middleName = middleName;
            this.maidenName = maidenName;
            this.lastName = lastName;
        }
        
        /**
         * @return the firstName
         */
        public Optional<String> getFirstName() {
            return Optional.ofNullable(firstName);
        }
        
        /**
         * @return the middleName
         */
        public Optional<String> getMiddleName() {
            return Optional.ofNullable(middleName);
        }
        
        /**
         * @return the lastName
         */
        public Optional<String> getMaidenName() {
            return Optional.ofNullable(maidenName);
        }
        
        /**
         * @return the lastName
         */
        public Optional<String> getLastName() {
            return Optional.ofNullable(lastName);
        }
        
    }
    
}
