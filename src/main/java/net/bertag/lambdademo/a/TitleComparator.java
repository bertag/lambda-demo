package net.bertag.lambdademo.a;

import java.util.Comparator;

public class TitleComparator implements Comparator<Book> {

    public int compare(Book a, Book b) {
        String aTitle = a.getTitle() == null ? "" : a.getTitle();
        String bTitle = b.getTitle() == null ? "" : b.getTitle();
        
        return aTitle.compareTo(bTitle);
    }
    
}
