package net.bertag.lambdademo.a;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class OldSchool {
    
    public static void main(String[] args) {
        // Initialize an unordered list of books.
        List<Book> books = new ArrayList<>();
        books.add(new Book("Harry Potter and the Sorcerer's Stone", "Rowling, J.K."));
        books.add(new Book("The Hunger Games", "Collins, Suzanne"));
        books.add(new Book("Ender's Game", "Card, Orson Scott"));
        books.add(new Book("The Case for Mars", "Zubrin, Robert"));
        
        // Sort the books by title, by instantiating a predefined Comparator implementation.
        System.out.println("Sorted by Title:");
        books.sort(new TitleComparator());
        printBookList(books);
        
        // Sort the books by author, by instantiating and defining an anonymous Comparator implementation.
        System.out.println("Sorted by Author:");
        books.sort(new Comparator<Book>() {
            @Override
            public int compare(Book a, Book b) {
                String aAuthor = a.getAuthor() == null ? "" : a.getAuthor();
                String bAuthor = b.getAuthor() == null ? "" : b.getAuthor();
                
                return aAuthor.compareTo(bAuthor);
            }
        });
        printBookList(books);
    }
    
    private static void printBookList(List<Book> books) {
        // Use a for-each loop to print each element in the book list.
        for (Book book : books) {
            System.out.println("\t" + book.getTitle() + " / " + book.getAuthor());
        }
        
        // Really old-school.
        // 
        // for (int i = 0; i < books.size(); i++) {
        //     Book book = books.get(i);
        //     System.out.println("\t" + book.getTitle() + " / " + book.getAuthor());
        // }

        // Really, *really* old-school.
        // 
        // Iterator<Book> iterator = books.iterator();
        // while(iterator.hasNext()) {
        //     Book book = iterator.next();
        //     System.out.println("\t" + book.getTitle() + " / " + book.getAuthor());
        // }
    }
    
}
