package net.bertag.lambdademo.a;

import java.util.ArrayList;
import java.util.List;

public class NewSchool {
    
    public static void main(String[] args) {
        // Initialize an unordered list of books.
        List<Book> books = new ArrayList<>();
        books.add(new Book("Harry Potter and the Sorcerer's Stone", "Rowling, J.K."));
        books.add(new Book("The Hunger Games", "Collins, Suzanne"));
        books.add(new Book("Ender's Game", "Card, Orson Scott"));
        books.add(new Book("The Case for Mars", "Zubrin, Robert"));
        
        // Sort the books by title, using a lambda expression (an anonymous method that satisfies the requirements of
        // the Comparator interface).
        System.out.println("Sorted by Title:");
        books.sort((Book a, Book b) -> {
            String aTitle = a.getTitle() == null ? "" : a.getTitle();
            String bTitle = b.getTitle() == null ? "" : b.getTitle();
            
            return aTitle.compareTo(bTitle);
        });
        printBookList(books);
        
        // Sort the books by author, using a shortened lambda expression referencing another method.
        System.out.println("Sorted by Author:");
        books.sort((a, b) -> nonNull(a.getAuthor()).compareTo(nonNull(b.getAuthor())));
        printBookList(books);
    }

    private static String nonNull(String str) {
        return str == null ? "" : str;
    }
    
    private static void printBookList(List<Book> books) {
        // Use a lambda expression to print each element in the book list.
        books.forEach(book -> System.out.println("\t" + book.getTitle() + " / " + book.getAuthor()));
    }
    
}
