# Part A

## What are "lambdas"?

The term "lambda" is taken from a branch of mathematics called "lambda calculus".  According to Wikipedia, "Lambda
calculus is a formal system in mathematical logic for expressing computation based on function abstraction and
application using variable binding and substitution" (https://en.wikipedia.org/wiki/Lambda_calculus).

It is worth pointing out that this branch of mathematics was developed by one Alonzo Church, who later served as Alan
Turing's Ph.D. advisor, and that lambda calculus is widely thought to present an equivalent mathematical definition of
"effective computability" as the later Turing Machine. 

Functional programming theory relies heavily on lambda calculus, and the term "lambda" has come to be associated with
this style of software development.  In essence, then, lambda expressions are really just functions, though in Java
(and several other languages), the term implies some expectation that they are used in a functional programming-style.

## Ok, so what does this mean for me as an Average Joe programmer? 

Because ultimately, lambda calculus is Turing complete, there's not anything magical that lambda expressions can do that
you haven't already seen done in other ways.  But there are some really nice benefits to the addition of lambdas to
Java 8.

The first is support for higher-order functions.  Higher order functions are functions which take other functions as
arguments, or return a function as a result.  This may still seem to be theoretical more than practical, but there are
some really cool implications for us as programmers.  We'll take a look at that later... 

The other main benefit of lambdas in Java is that they provide a really nice syntactic sugar for your programs.  Things
that required a lot of code before can now be done in a much less verbose way.  Let's take a closer look at this
benefit, since it is probably the easiest to visualize.

## Syntactic Sugar

Consider the **Comparable** and **Comparator** interfaces, which were both introduced in Java 1.2 with the Java
Collections Framework.

**Comparable** provides a universal mechanism for defining sort order between two objects of the same type based on
their "natural ordering".  Most of the number classes such as **Integer** and **Double** implement **Comparable**,
since numbers have an obvious natural ordering.  **String** also implements **Comparable**, basing it's natural
ordering on unicode values.

But what about objects that don't have an inherent, "natural" order?  For example, consider a **Book**.  It might be
equally appropriate to order a **Book** based on title, or author, or some other value (such as a call number).  The
**Comparator** interface provides a mechanism for defining custom, interchangeable sorting rules for these types of
objects which have no natural order.

In this package, we have defined a simple **Book** object, consisting solely of "title" and "author" attributes.  We
have also implemented several different **Comparator**s.  Look first at the **TitleComparator** class, which compares
books based on their title field.  In simple terms, we do this by deferring to the natural ordering of the comparable
**String** (of which "title" is an instance) for any two books `a` and `b`:

```a.getTitle().compareTo(b.getTitle())```

Some additional complexity is introduced to ensure that if one of the books does not have a title, that we can still
assume some ordering (in this case, by associating a `null` value with the empty string [""]). This code does not seem
complex nor verbose, and provides a pattern which can be replicated for other reasonable book sorting strategies.

Let's see it in the context of our example application then.  Opening "OldSchool.java", you will find a `main` method
that populates a list of 4 books and then sorts them.  Two **Comparator**s are used, one to sort the books by title,
the other by author.  Different mechanism for implementing **Comparator** are presented.  To sort titles, we simply
instantiate the **TitleComparator** previously presented.  Alternatively, we also can define an anonymous class which
implemements **Comparator**, a pattern used rather widely in the AWT and Java Swing communities to build user
interfaces.  This pattern is used to compare books by "author".

Now let's consider how we might be able to use lambdas to simplify our code.  Note that the **Comparator** interface
defines only 1 abstract method (`compare`).  This is true for several other common interfaces as well:

* **Comparable** (`compareTo`)
* **Runnable** (`run`)
* **Callable** (`call`)
* **ActionListener** (`actionPerformed`)
* and others...

For these interfaces the "contract", or guarantees that they offer, is fully satisfied by the implementation of 
one single method.  Thus, the implementation of that method alone in isolation could reasonably be viewed as a drop-in
replacement for a formal implementation of an entire object which implements that same interface. This idea is the
heart and soul of how lambdas work in Java.  Lambda expressions provide us the mechanism for defining functions in
place of interface implementations.  And doing things that way proves to be much less verbose.

Open "NewSchool.java", which provides a lambda-based solution for sorting books identical to the one in
"OldSchool.java".  Again, we present the title and author solution slightly differently to show variations of the form.
The `sort` method on our "books" list accepts a **Comparator** implementation as its argument, but instead we have
defined the sorting logic in-line as a lambda expression.

A side by side comparison of **TitleComparator** and this expresssion shows the similarities between a formal
implementation of `compare` and the lambda.  In both cases, we define two arguments, `Book a` and `Book b`, and a
function body which returns an `int` in accordance with the **Comparator** contract.  In fact, the most obvious
difference is simply the use of the new arrow syntax (`->`) to divide the arguments from the function body. Presented
in this way, there is really not that much space saved compared to the in-line **Comparator** implementation
demonstrated at the end of the **OldSchool** `main` method.  But I have deliberately demonstrated the longest form of
lambda possible; in reality, Java offers several ways to make lambdas much, much shorter!

Now observe the lambda expression for comparing books using authors in the **NewSchool** `main`.  Note that in this
case, we resolve the entire method in a single line of code by off-loading the logic involved in resolving the `null`
case to another method.  When lambdas can be reduced to a single line of code, curly braces ("{ }") are no longer
required around the function body, and the `return` keyword becomes implied.  Additionally, we let the JVM infer
the types of our arguments based on the context.  In this form, the implementation of **Comparator** is noticeably
reduced from its original form.

## Functions everywhere

Several new "universal" interfaces were defined in Java 8 to provide a standard set of templates for functional
expressions.  These new interfaces are defined in the java.util.function package
(https://docs.oracle.com/javase/8/docs/api/java/util/function/package-summary.html) and appear widely throughout Java 8.
Like existing interfaces such as **Comparator** these new "functional interfaces" can be implemented formally by a
concrete class.  But unlike interfaces such as **Comparator** which by its name and documentation has a clear, specific
use-case, these new interfaces don't provide any contextual restrictions as to what they might accomplish or when they
should be used.

*Note that just like any other interface, you could create a concrete class which implements these functional
interfaces, but obviously they are not meant to be used that way.*

Why are these new interfaces important?  Well consider that in Java, methods are uniquely identified by *class, name,
argument types and order, and return type*.  So the single abstract method in **Comparator** is defined as follows:

```
int Comparator<T>.compare(T, T)
```

Similarly, the single abstract method in the new **BiFunction** functional interface has the following definition:

```
V BiFunction<T, U, V>.apply(T, U)
```

Since lambdas are defined as anonymous, in-line functions rather than named methods attached to classes, the definition
is reduced to *argument types and order, and return type*.  In this anonymous context then, a lambda expression that
satisfies **Comparator<Book>** also simultaneously satisfies **BiFunction<Book, Book, Integer>** (which should be read
as a function that accepts two **Book**s as input, and returns an **Integer** as output)!  This allows us an
unparalleled amount of flexibility when writing lambda expressions.  A single lambda expression could potentially
satisfy a wide variety of interface requirements, and as a result, many new methods and classes added to Java 8 define
functional interfaces as arguments.  

## Another demonstration of lambda-based syntactic sugar

In both "OldSchool.java" and "NewSchool.java", we defined a method, `printBookList` that is responsible for formatting
and printing the list of books in a nice, human-readable form.  In "OldSchool.java", I demonstrated 3 possible ways
that this could be done.  The cleanest is to use the enhanced "foreach" loop introduced in Java 1.5 (3 lines of code).
But we can also use an iterator-based for-loop (4 lines of code), or if we're feeling particularly nostalgic, a
while-loop using the list iterator (5 lines of code).

Contrast these solutions to the `printBookList` method in "NewSchool.java", which uses the `forEach` method defined
in **Collection** to perform the same task in only 1 line of code.  This `forEach` method requires a single argument,
a **Consumer<?>** (a function that accepts an object of any type, though in this case restricted to only the type of
object contained in our **Book** list, and returns no value).  In satisfaction of this requirement, we provide a lambda
expression implementing the **Consumer** interface by calling printing the title and author of a given book. Behind the
scenes, our list knows how to iterate through itself, and calls this lambda expression on each and every element inside.

This is a pretty dramatic re-imagining of how collections are interacted with, but one which is extremely powerful.
While this example is very simple, in later lessons, we will look at how we can use this concept to completely
overhaul how we deal with complex collections of complex objects.  How we can take annoying "papercuts" in traditional
Java programming and turn them into something elegant.